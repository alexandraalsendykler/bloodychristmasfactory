﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Fr.EQL.AI110.ChristmasFactory.DataAccess;
using Fr.EQL.AI110.ChristmasFactory.Entities;

namespace Fr.EQL.AI110.ChristmasFactory.TestApp
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            //TestInsertion();
            //TestMiseAJour();
            TestSuppression();
        }

        public static void TestSuppression()
        {
            Console.WriteLine("id à supprimer ?");
            int id = int.Parse(Console.ReadLine());
            ElfDAO dao = new ElfDAO();
            dao.Delete(id);
        }

        public static void TestMiseAJour()
        {
            Console.WriteLine("id à modifier ?");
            int id = int.Parse(Console.ReadLine());
            Console.WriteLine("Nouveau nom : ");
            string nom = Console.ReadLine();

            Elf e = new Elf(id, nom, true, "plop.jpg");  

            ElfDAO dao = new ElfDAO();
            dao.Update(e);

        }

        public static void TestInsertion()
        {
            Console.Write("Nom de l'elfe ? ");
            string nom = Console.ReadLine();
            Elf e = new Elf(0, nom, true, "dobbie.jpg");
            ElfDAO dao = new ElfDAO();
            dao.Insert(e);
        }
    }
}
