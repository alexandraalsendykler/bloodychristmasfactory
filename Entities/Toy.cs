﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.ChristmasFactory.Entities
{
    public class Toy
    {
       
        #region ATTRIBUTES
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
        // nullable int :
        public int? IdResponsable { get; set; }
        #endregion

        #region CONSTRUCTORS
        public Toy()
        {

        }
        public Toy(int id, string name, string description, float price, int idResponsable)
        {
            Id = id;
            Name = name;
            Description = description;
            Price = price;
            IdResponsable = idResponsable;
        }

        #endregion
    }
}
