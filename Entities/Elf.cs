﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.ChristmasFactory.Entities
{
    public class Elf
    {
        #region ATTRIBUTES
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsAvailable { get; set; }
        public string Picture { get; set; }
        #endregion

        #region CONSTRUCTORS
        public Elf()
        {
        }

        public Elf(int id, string name, bool isAvailable, string picture)
        {
            Id = id;
            Name = name;
            IsAvailable = isAvailable;
            Picture = picture;
        }
        #endregion


    }
}
